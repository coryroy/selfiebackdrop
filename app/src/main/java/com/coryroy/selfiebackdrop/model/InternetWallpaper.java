package com.coryroy.selfiebackdrop.model;

import lombok.Getter;

/**
 * Created by User on 10/26/2014.
 */
public class InternetWallpaper {
    @Getter String url;
    @Getter String title;
    @Getter BackdropCategory category;

    public InternetWallpaper(String url, String title, BackdropCategory category) {
        this.url = url;
        this.title = title;
        this.category = category;
    }
}
