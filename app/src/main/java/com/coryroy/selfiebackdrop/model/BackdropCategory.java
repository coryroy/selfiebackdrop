package com.coryroy.selfiebackdrop.model;

/**
 * Created by User on 10/26/2014.
 */
public enum BackdropCategory {
    CITY,
    NATURE,
    SPACE
}
