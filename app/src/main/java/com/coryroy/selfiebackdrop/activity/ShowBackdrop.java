package com.coryroy.selfiebackdrop.activity;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;

import com.coryroy.selfiebackdrop.R;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class ShowBackdrop extends Activity {
    public static final String EXTRA_URL = "backdrop_url";
    @InjectView(android.R.id.background) ImageView backdrop;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_backdrop);
        ButterKnife.inject(this);
        getIntent().getStringExtra(EXTRA_URL);
        Picasso.with(this).load(getIntent().getStringExtra(EXTRA_URL)).into(backdrop);
    }

}
