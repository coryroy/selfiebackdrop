package com.coryroy.selfiebackdrop.activity;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v17.leanback.app.BrowseFragment;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.BaseCardView;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ImageCardView;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.Presenter;
import android.view.View;
import android.view.ViewGroup;

import com.coryroy.selfiebackdrop.R;
import com.coryroy.selfiebackdrop.model.BackdropCategory;
import com.coryroy.selfiebackdrop.model.InternetWallpaper;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class BackdropChooser extends Activity {

    public static final String TAG = "BrowseActivity";
    private static final int NUM_ROWS = BackdropCategory.values().length;

    protected BrowseFragment mBrowseFragment;
    private ArrayObjectAdapter mRowsAdapter;

    private ArrayList<InternetWallpaper> backdrops = new ArrayList<InternetWallpaper>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backdrop_chooser);
        backdrops.add(new InternetWallpaper("http://wallpaperswiki.org/wp-content/uploads/2012/10/Late-Summer-Field-Summer-Landscape-.jpg", "Summer Field", BackdropCategory.NATURE));
        backdrops.add(new InternetWallpaper("http://www.mrwallpaper.com/wallpapers/tokyo-tower-night.jpg", "Tokyo Tower", BackdropCategory.CITY));
        backdrops.add(new InternetWallpaper("http://neworleanseasytravelguide.com/wp-content/uploads/2012/12/bourbon-street-at-night.jpg", "New Orleans at Night", BackdropCategory.CITY));
        backdrops.add(new InternetWallpaper("http://static.guim.co.uk/sys-images/Guardian/Pix/space/2014/1/31/1391174912198/Earth-rising-over-moon-014.jpg", "Earth from the Moon", BackdropCategory.SPACE));
        backdrops.add(new InternetWallpaper("http://expedino.com/wp-content/uploads/2013/12/Beautiful-Dreamy-Fantasy-Outer-Space-Wallpaper.jpg", "Ice World", BackdropCategory.SPACE));

        final FragmentManager fragmentManager = getFragmentManager();
        mBrowseFragment = (BrowseFragment) fragmentManager.findFragmentById(
                R.id.browse_fragment);

        // Set display parameters for the BrowseFragment
        mBrowseFragment.setHeadersState(BrowseFragment.HEADERS_ENABLED);
        mBrowseFragment.setTitle(getString(R.string.app_name));
        mBrowseFragment.setBadgeDrawable(getResources().getDrawable(
                R.drawable.app_icon_quantum));

        mBrowseFragment.setHeadersState(BrowseFragment.HEADERS_ENABLED);
        mBrowseFragment.setHeadersTransitionOnBackEnabled(true);
        // set fastLane (or headers) background color
        mBrowseFragment.setBrandColor(getResources().getColor(R.color.fastlane_background));
        // set search icon color
        mBrowseFragment.setSearchAffordanceColor(getResources().getColor(R.color.search_opaque));
        // mBrowseFragment.setBrowseParams(params);

        buildRowsAdapter();
    }

    private void buildRowsAdapter() {
        mRowsAdapter = new ArrayObjectAdapter(new ListRowPresenter());

        for (int i = 0; i < NUM_ROWS; ++i) {
            ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(
                    new CardPresenter());

            for (InternetWallpaper iw : backdrops) {
                if (BackdropCategory.values()[i].equals(iw.getCategory()))
                    listRowAdapter.add(iw);
            }
            HeaderItem header = new HeaderItem(i, BackdropCategory.values()[i].name(), null);
            mRowsAdapter.add(new ListRow(header, listRowAdapter));
        }

        mBrowseFragment.setAdapter(mRowsAdapter);
    }

    public class CardPresenter extends Presenter {

        public static final int WIDTH = 400;
        public static final int HEIGHT = 325;

        public ViewHolder onCreateViewHolder(ViewGroup parent) {
            Context c = BackdropChooser.this;
            ImageCardView cardView = new ImageCardView(c, null);
            cardView.findViewById(R.id.title_text).setVisibility(View.VISIBLE);
            BaseCardView.LayoutParams params = new BaseCardView.LayoutParams(WIDTH, HEIGHT);
            cardView.setLayoutParams(params);
            cardView.setFocusable(true);
            return new ViewHolder(cardView);
        }

        public void onBindViewHolder(ViewHolder viewHolder, Object item) {
            final InternetWallpaper iw = (InternetWallpaper) item;
            final ImageCardView icw = (ImageCardView) viewHolder.view;

            icw.setTitleText(iw.getTitle());
            Picasso.with(BackdropChooser.this).load(iw.getUrl()).resize(WIDTH, HEIGHT - 75).into(icw.getMainImageView());
            viewHolder.view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(BackdropChooser.this, ShowBackdrop.class);
                    intent.putExtra(ShowBackdrop.EXTRA_URL, iw.getUrl());
                    startActivity(intent);
                }
            });
        }

        public void onUnbindViewHolder(ViewHolder viewHolder) {
            // no op
        }
    }


}
