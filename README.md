# Selfie Backdrop #

This is a small Nexus Player app that I built for a Vancouver DevFest 2014.  The idea is to show off how to use BrowseFragment and CardView and make a fun app to use as a backdrop for selfies - as long as you have a giant TV.

This repo is the solution to the lab.

lab1 - get an image to display on the screen
lab2 - show a list of images with browse fragment by category and allow user to pick it to display
lab3 - use a cardview to show preview of image inside browsefragment

The branches (lab1,lab2,lab3) are the solutions.

You can do this lab without a TV as long as you have a device or emulator running API level 17 (android 4.2) or above.

### Who do I talk to? ###

* Cory Roy (corymroy@gmail.com)
